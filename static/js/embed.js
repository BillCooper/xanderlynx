var embed = {};

var generateYoutubeEmbed = function(_, vid, qs) {
  return "https://www.youtube-nocookie.com/embed/" + vid + "?" + qs;
};

var generateInvidiousEmbed = function(_, vid, qs) {
  return "https://invidio.us/embed/" + vid + "?" + qs;
}

var embedMap = new Map([
  [/youtube\.com\/watch\?v=([^&]+)(?:&(.*))?$/, generateYoutubeEmbed],
  [/youtu\.be\/([^?]+)(?:\?(.*))?$/, generateYoutubeEmbed],
  [/invidio\.us\/watch\?v=([^&]+)(?:&(.*))?$/, generateInvidiousEmbed],
]);

embed.generateEmbedHTML = function(link) {
  return '<iframe width="480" height="270" ' +
    'src="' + link + '" frameborder="0" allowfullscreen></iframe>';
}

embed.init = function() {

  var messages = document.getElementsByClassName('divMessage');

  for (var i = 0; i < messages.length; i++) {

    var links = messages[i].getElementsByTagName('a');

    for (var j = 0; j < links.length; j++) {
      embed.processLinkForEmbed(links[j]);
    }

  }

};

embed.processLinkForEmbed = function(link) {

  Array.from(embedMap.entries()).some(function(data) {
    var regexp = data[0],
        f = data[1];

    if (!regexp.test(link.href)) return false;

    link.style.display = "inline";

    var embedLink = f.apply(null, link.href.match(regexp));
    var iframe = embed.generateEmbedHTML(embedLink);

    var embedCtr = document.createElement("div");
    embedCtr.style.display = "inline";

    var embedButton = document.createElement("span");
    embedButton.textContent = "[Embed]";
    embedButton.className = "embedButton glowOnHover";
    embedCtr.append(embedButton);

    var iframeCtr = document.createElement("div");
    iframeCtr.style.display = "none";
    embedCtr.append(iframeCtr);

    var active = false;
    embedButton.addEventListener("click", function() {
      active = !active;

      if (active) {
        iframeCtr.style.display = "block";
        embedButton.textContent = "[Remove]";
        iframeCtr.innerHTML = iframe;
      } else {
        iframeCtr.style.display = "none";
        embedButton.textContent = "[Embed]";
        iframeCtr.innerHTML = "";
      }
    });

    link.parentElement.insertBefore(embedCtr, link.nextSibling);

    return true;
  });
};

embed.init();
